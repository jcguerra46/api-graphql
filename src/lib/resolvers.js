'use strict'

const courses = [
    {
        _id: 'anyid',
        title: 'My Title',
        teacher: 'My Teacher',
        description: 'The description',
        topic: 'the topic'
    },
    {
        _id: 'anyid2',
        title: 'My Title 2',
        teacher: 'My Teacher 2',
        description: 'The description 2',
        topic: 'the topic 2'
    },
    {
        _id: 'anyid3',
        title: 'My Title 3',
        teacher: 'My Teacher 3',
        description: 'The description 3',
        topic: 'the topic 3'
    }
]

module.exports = {
    getCourses: () => {
        return courses
    }
}
